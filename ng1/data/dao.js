'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'contacts';

class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    getAll() {
        return this.db.collection(COLLECTION).find().toArray();
    }

    getById(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).findOne({ _id: id});
    }

    update(id, data){
        id = new ObjectID(id);
        data._id = id;
        return this.db.collection(COLLECTION).updateOne({ _id: id}, data);
    }

    add(data){
        return this.db.collection(COLLECTION).insertOne(data);
    }

    removeById(id){
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).remove({ _id: id});
    }

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports = Dao;
