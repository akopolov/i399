'use strict'

const express = require ('express');
const bodyParser = require('body-parser');
const app = express();
const Dao = require('./dao.js');

app.use(bodyParser.json());

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.post('/api/contacts', postContact);
app.delete('/api/contacts/:id', removeContact)
app.post('/api/contacts/delete', removeContacts)
app.put('/api/contacts/:id', putContacts)

var url = 'mongodb://user1:asdasd@ds133981.mlab.com:33981/i399';
var dao = new Dao();

dao.connect(url)
    .then( () => app.listen(3000));

function getContacts(request, response) {
    response.set('Content-Type','application/json');
    dao.getAll().then(data => {
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}

function getContact(request, response) {
    response.set('Content-Type','application/json');
    dao.getById(request.params.id).then(data => {
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}

function postContact(request, response) {
    response.set('Content-Type','application/json');
    dao.add(request.body).then(data => {
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}

function removeContact(request, response) {
    response.set('Content-Type','application/json');
    dao.removeById(request.params.id).then(data => {
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}

function removeContacts(request, response) {
    response.set('Content-Type','application/json');
    var actions = request.body.map(function (item) {
        return dao.removeById(item);
    });
    Promise.all(actions).then(data =>{
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}

function putContacts(request, response) {
    response.set('Content-Type','application/json');
    dao.update(request.params.id,request.body).then(data => {
        response.end(JSON.stringify(data))
    }).catch(error => {
        console.log(error);
        response.end('Error' + error);
    });
}