(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', SearchCtrl);

    function SearchCtrl($http, navService, modalService){
        var vm = this;
        this.removeItem = removeItem;
        this.removeList = removeList;
        this.items = [];

        navService.initNav('menu-search');
        init();

        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.items = result.data
            });
        }
        
        function removeItem(id) {
            modalService.confirm().then(function () {
                return $http.delete('api/contacts/' + id);
            }).then(init);
        }
        
        function removeList() {
            var items = vm.items.filter(function (element) {
                if(element.selected === true){
                    return element;
                }
            });

            if(items.length > 0){
                modalService.confirm().then(function () {
                    items.forEach(function (element) {
                        $http.delete('api/contacts/' + element._id);
                    });
                }).then(init);
            }
        }
    }
})();
