(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', EditCtrl);

    function EditCtrl($http, $routeParams, $location, navService) {
        var vm = this;
        vm.item = {}
        this.updateItem = updateItem;

        navService.initNav(null);

        $http.get('api/contacts/' + $routeParams.id).then(function (result) {
            vm.item = result.data;
        });
        
        function updateItem(item) {
            $http.put('api/contacts/'+item._id,item).then(function () {
                $location.path('/search');
            });
        }
    }
})();