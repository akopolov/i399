(function () {
    'use strict';

    angular.module('app').controller('NewCtrl', NewCtrl);
    
    function NewCtrl($http,$location, navService) {
        var vm = this;
        this.addItem = addItem;
        this.newName="";
        this.newPhone="";

        navService.initNav('menu-new');

        function addItem() {
            var newItem = {
                name: vm.newName,
                phone: vm.newPhone,
            };

            $http.post('api/contacts',newItem).then(function () {
                $location.path('/search');
            });
        }
    }
})();
