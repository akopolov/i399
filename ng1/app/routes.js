(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl : 'app/views/search.html',
            controller : 'SearchCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/views/new.html',
            controller : 'NewCtrl',
            controllerAs : 'vm'
        }).when('/edit/:id', {
            templateUrl : 'app/views/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm'
        }).otherwise('/search');

    }
})();