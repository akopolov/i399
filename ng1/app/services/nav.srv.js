(function () {
    'use strict';

    angular.module('app').service('navService', NavSrv);

    function NavSrv() {

        this.initNav = function (path){
            var ul = document.getElementById('menu').children;

            for(var i = 0; i < ul.length; i++){
                ul[i].children[0].className="";
            }
            if(path != null){
                document.getElementById(path).className = "active";
            }
        }
    }
})();
