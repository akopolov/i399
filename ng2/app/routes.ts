import { Routes } from '@angular/router';
import { ListComponent } from './search/search.cmp';
import { EditComponent } from './edit/edit.cmp';
import {AddComponent} from "./new/new.cmp";

export const routes: Routes = [
    { path: 'search', component: ListComponent },
    { path: 'new', component: AddComponent },
    { path: 'edit/:id', component: EditComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];