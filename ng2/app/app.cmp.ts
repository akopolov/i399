import { Component } from '@angular/core';
import {Task, TaskService} from "./task.srv";


@Component({
    selector: 'my-app',
    templateUrl: 'app/app.html',
    styleUrls: ['app/search/search.css']
})
export class AppComponent {
    name : string = '';
    phone : string = '';
    tasks : Task[] = [];

    constructor(private taskService: TaskService) {
        this.updateTasks();
    }

    updateTasks() {
        this.taskService.getTasks()
            .then(tasks => this.tasks = tasks)
    }
    addNewTask() {
        this.taskService.saveTask(new Task(this.name,this.phone))
            .then(() => {
                this.updateTasks();
                this.name = '';
                this.phone = '';
            });
    }
}
