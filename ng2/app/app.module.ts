import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.cmp';
import { TaskService } from "./task.srv";
import { RouterModule } from "@angular/router";
import { routes } from "./routes";
import { ListComponent } from "./search/search.cmp";
import { EditComponent } from "./edit/edit.cmp";
import { AddComponent } from "./new/new.cmp";
import { FilterPipe } from "./search/filter.cmp";

@NgModule({
    imports: [ BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot(routes, { useHash: true })],
    declarations: [ AppComponent,
        ListComponent,
        EditComponent,
        AddComponent,
        FilterPipe],
    providers: [ TaskService],
    bootstrap: [ AppComponent ]
})
export class AppModule { }