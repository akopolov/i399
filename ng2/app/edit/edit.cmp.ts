import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Task, TaskService} from '../task.srv';

@Component({
    templateUrl: 'app/edit/edit.html'
})
export class EditComponent implements OnInit {

    task: Task;
    title: string = '';
    phone: string = '';
    id: number = 0;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private taskService: TaskService) {}


    update(): void {
        let task = new Task(this.title, this.phone);
        task._id = this.id;
        this.taskService.updateTask(task)
            .then(() => {
                this.back();
            });
    }

    back() {
        this.router.navigateByUrl('/search');
    }

    setData(task : Task){
        this.title = task.title;
        this.phone = task.phone;
        this.id = task._id;
    }

    ngOnInit():void {
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.taskService.getTask(id)
            .then(task => this.setData(task))
    }
}
