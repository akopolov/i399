import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Task, TaskService} from '../task.srv';

@Component({
    templateUrl: 'app/new/new.html'
})
export class AddComponent{

    task: Task;
    title: string = '';
    phone: string = '';

    constructor(private router: Router,
                private taskService: TaskService) {}

    add(): void {
        let task = new Task(this.title, this.phone);
        this.taskService.saveTask(task)
            .then(() => {
                this.back();
            });
    }

    back() {
        this.router.navigateByUrl('/search');
    }
}
