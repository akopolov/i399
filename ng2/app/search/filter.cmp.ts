import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../task.srv';

@Pipe({
    name: 'filter',
    pure: false
})

export class FilterPipe implements PipeTransform {
    transform(value: Task[], args: string){
        if(args == null ) return value;
        let filter = args.toLocaleLowerCase();
        return filter ? value.filter(item => item.title.toString().toLocaleLowerCase().indexOf(filter) + 1 ||
                                             item.phone.toString().toLocaleLowerCase().indexOf(filter) + 1) : value;
    }
}