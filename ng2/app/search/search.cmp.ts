import { Component, OnInit } from '@angular/core';
import { Task, TaskService } from '../task.srv';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
    selector: 'list',
    templateUrl: 'app/search/search.html',
    styleUrls: ['app/search/search.css'],
})
export class ListComponent implements OnInit {
    tasks: Task[] = [];

    constructor(private taskService: TaskService) {}

    private updateTasks(): void {
        this.taskService.getTasks().then(tasks => this.tasks = tasks);
    }

    deleteTask(taskId : number): void {
        this.taskService.deleteTask(taskId)
            .then(() => this.updateTasks());
    }

    removeList(){
        this.tasks.filter(item => {if(item.done) this.taskService.deleteTask(item._id)});
        this.updateTasks();
    }

    ngOnInit(): void {
        this.updateTasks();
    }
}
